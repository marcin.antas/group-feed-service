# Group Feed Rest API

Sample REST API for posting content in groups.

## Prerequisities:

1. Java 8
2. Scala 2.12
3. Maven 3+

Optionally if one wants to run project using docker containers one must install:

4. Docker
5. Docker compose

## Usage:

In order to build project one must issue:

```bash
mvn clean install
```

## Running project:

The simplest way to start the project is to run **group.feed.service.AppBoot** class in IntelliJ.

In this configuration REST service will start with H2 Database.

## Working with docker compose:

In order to run the project with a PostgreSQL database one can use docker-compose.

The simplest way to do it is to run **mvn clean package docker-compose:up** command.

```shell
mvn clean package docker-compose:up
```

Then the group feed service api is accessible under this address: **http://localhost:8080**

## REST API

### Groups

#### Get User's Groups

```bash
curl -X "GET" -H "Auth: {user_id}" "http://localhost:8080/api/v1/groups"
```
#### Add User to Group

```bash
curl -X "POST" -H "Auth: {user_id}" "http://localhost:8080/api/v1/groups/{id}/add"
```

#### Create Post in Group

```bash
curl -X "POST" "http://localhost:8080/api/v1/groups/{id}/posts" \
       -H "Auth: {user_id}" \
       -H "Content-Type: application/json" \
       -d $'{ "content": "First post in Travel group" }'
```

#### Get Group's Posts

```bash
curl -X "GET" -H "Auth: {user_id}" "http://localhost:8080/api/v1/groups/{id}/posts"
```

### Posts

### Get All Posts (that User has access to)

```bash
curl -X "GET" -H "Auth: {user_id}" "http://localhost:8080/api/v1/posts"
```

## Choices:

I have chosen relational database here in this example in order to present the way 
how can the groups and feeds be implemented using relational database and queried using Slick to support non-blocking and asynchronous.

## Performance note:

Posts table can get very big when a lot of people post their activity.

In order to sustain the same performance one can do:

1. Posts table can be partitioned for faster fetch data operation.

2. Database server can be run in cluster configuration (for example db can consist of 4 nodes in cluster, 1 node for read/write operations the rest 3 nodes configured only for read operations)

3. Database schema can be tweaked to create one posts table for one year of posts, so that one posts table will store data just for given year.

## What's missing:

1. Validation and error handling of the application errors
2. REST API tests
