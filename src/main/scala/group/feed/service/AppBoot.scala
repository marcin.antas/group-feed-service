package group.feed.service

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import group.feed.service.http.{GroupRoutes, PostRoutes}
import group.feed.service.service.{GroupService, PostService}
import com.typesafe.config.ConfigFactory

object AppBoot extends App {
  implicit val system = ActorSystem()
  implicit val executor = system.dispatcher
  implicit val materializer = ActorMaterializer()
  // config / logger
  val config = ConfigFactory.load()
  val logger = Logging(system, getClass)
  // services
  val groupService = GroupService()
  val postService = PostService()
  // routes
  val groupRoutes = GroupRoutes(groupService, postService)
  val postRoutes = PostRoutes(postService)
  val routes = groupRoutes.route ~ postRoutes.route
  // app
  val port = if (sys.env.contains("PORT")) sys.env("PORT").toInt else config.getInt("http.port")
  Http().bindAndHandle(routes, config.getString("http.interface"), port)
}
