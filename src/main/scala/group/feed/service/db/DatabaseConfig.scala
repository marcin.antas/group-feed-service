package group.feed.service.db

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import slick.jdbc.PostgresProfile

object DatabaseConfig {

  val dbname = if (System.getenv("DB_NAME") != null) System.getenv("DB_NAME") else "testdb"
  val dbConfig = ConfigFactory.load("db-application").getConfig(dbname)

  private val hikariConfig = new HikariConfig()
  hikariConfig.setJdbcUrl(dbConfig.getString("url"))
  hikariConfig.setUsername(dbConfig.getString("user"))
  hikariConfig.setPassword(dbConfig.getString("password"))

  val dataSource = new HikariDataSource(hikariConfig)
  val jdbcProfile = PostgresProfile

  import jdbcProfile.api._
  val db = Database.forDataSource(dataSource, Option(10))

  object tables extends Tables(PostgresProfile)

  db.createSession()

  db.run(tables.groups.schema.createIfNotExists)
  db.run(tables.userGroups.schema.createIfNotExists)
  db.run(tables.posts.schema.createIfNotExists)
}
