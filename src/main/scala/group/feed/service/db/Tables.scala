package group.feed.service.db

import group.feed.service.model.{Group, Post, UserGroup}
import org.joda.time.DateTime
import slick.jdbc.JdbcProfile

abstract class Tables(val jdbcProfile: JdbcProfile) {

  import jdbcProfile.api._
  import com.github.tototoshi.slick.H2JodaSupport._

  class Groups(tag: Tag) extends Table[Group](tag, "groups") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def * = (id, name) <> (Group.tupled, Group.unapply)
  }
  lazy val groups = TableQuery[Groups]

  class Posts(tag: Tag) extends Table[Post](tag, "posts") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def content = column[String]("content")
    def createDate = column[DateTime]("create_date")
    def authorUserId = column[Long]("author_user_id")
    def authorName = column[String]("author_name")
    def groupId = column[Long]("group_id")
    def group = foreignKey("groups", groupId, groups)(_.id)
    def * = (id.?, content, createDate.?, authorUserId.?, authorName.?, groupId.?) <> (Post.tupled, Post.unapply)
  }
  lazy val posts = TableQuery[Posts]

  class UserGroups(tag: Tag) extends Table[UserGroup](tag, "user_groups") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def userId = column[Long]("user_id")
    def groupId = column[Long]("group_id")
    def * = (id.?, userId, groupId) <> (UserGroup.tupled, UserGroup.unapply)
  }
  lazy val userGroups = TableQuery[UserGroups]
}
