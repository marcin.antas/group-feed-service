package group.feed.service.http

import akka.http.scaladsl.server.Directives._
import group.feed.service.json.{GroupJsonProtocol, PostJsonProtocol, UserGroupJsonProtocol}
import group.feed.service.model.Post
import group.feed.service.service.{GroupService, PostService}

class GroupRoutes(val groupService: GroupService, val postService: PostService)
  extends GroupJsonProtocol with PostJsonProtocol with UserGroupJsonProtocol {

  val route = logRequestResult("GroupRoutes") {
    headerValue(extractAuthUserId) { implicit user =>
      pathPrefix("api" / "v1" / "groups") {
        pathEndOrSingleSlash {
          get {
            complete {
              groupService.getGroups()
            }
          }
        } ~
          pathPrefix(LongNumber) { groupId =>
            path("posts") {
              get {
                complete {
                  postService.getGroupPosts(groupId)
                }
              } ~
                post {
                  entity(as[Post]) { newPost =>
                    complete {
                      postService.createPostInGroup(newPost, groupId)
                    }
                  }
                }
            } ~
              path("add") {
                post {
                  complete {
                    groupService.addUserToGroup(groupId)
                  }
                }
              }
          }
      }
    }
  }
}

object GroupRoutes {
  def apply(groupService: GroupService, postService: PostService) = new GroupRoutes(groupService, postService)
}