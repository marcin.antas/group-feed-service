package group.feed.service.http

import akka.http.scaladsl.server.Directives.{complete, get, logRequestResult, pathEndOrSingleSlash, pathPrefix, _}
import group.feed.service.json.PostJsonProtocol
import group.feed.service.service.PostService

class PostRoutes(val postService: PostService) extends PostJsonProtocol {

  val route = logRequestResult("PostRoutes") {
    headerValue(extractAuthUserId) { implicit user =>
      pathPrefix("api" / "v1" / "posts") {
        pathEndOrSingleSlash {
          get {
            complete {
              postService.getAllGroupsPosts()
            }
          }
        }
      }
    }
  }
}

object PostRoutes {
  def apply(postService: PostService) = new PostRoutes(postService)
}