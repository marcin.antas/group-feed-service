package group.feed.service

import akka.http.scaladsl.model.HttpHeader
import group.feed.service.model.User

package object http {
  val auth = "Auth".toLowerCase
  def extractAuthUserId: HttpHeader => Option[User] = {
    case HttpHeader(`auth`, value) => Some(User(value.toLong, s"username$value"))
    case _ => None
  }
}
