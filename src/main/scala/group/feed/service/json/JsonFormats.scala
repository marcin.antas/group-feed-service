package group.feed.service.json

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormatter, ISODateTimeFormat}
import spray.json.{DeserializationException, JsString, JsValue, RootJsonFormat}

trait JsonFormats {

  implicit object dateTimeIso8601Format extends RootJsonFormat[DateTime] {
    private val parserISO: DateTimeFormatter = ISODateTimeFormat.dateTime

    override def write(obj: DateTime) = JsString(parserISO.print(obj))

    override def read(json: JsValue): DateTime = json match {
      case JsString(s) => parserISO.parseDateTime(s)
      case _ => throw new DeserializationException(s"Cannot deserialize date $json")
    }
  }
}

