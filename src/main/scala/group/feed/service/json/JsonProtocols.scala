package group.feed.service.json

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import group.feed.service.model.{Group, Post, UserGroup}
import spray.json.DefaultJsonProtocol

trait PostJsonProtocol extends SprayJsonSupport with DefaultJsonProtocol with JsonFormats {
  implicit val postFormat = jsonFormat6(Post)
}

trait GroupJsonProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val groupFormat = jsonFormat2(Group)
}

trait UserGroupJsonProtocol extends SprayJsonSupport with DefaultJsonProtocol with JsonFormats {
  implicit val userGroupFormat = jsonFormat3(UserGroup)
}
