package group.feed.service.model

case class Group(id: Long, name: String)
