package group.feed.service.model

import org.joda.time.DateTime

case class Post(id: Option[Long] = None,
                content: String,
                createDate: Option[DateTime],
                authorUserId: Option[Long],
                authorName: Option[String],
                groupId: Option[Long])
