package group.feed.service.model

case class User(id: Long, name: String)
