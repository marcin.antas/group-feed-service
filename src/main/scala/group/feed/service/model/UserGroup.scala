package group.feed.service.model

case class UserGroup(id: Option[Long] = None, userId: Long, groupId: Long)
