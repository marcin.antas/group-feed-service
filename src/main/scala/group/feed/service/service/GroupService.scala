package group.feed.service.service

import group.feed.service.model.{Group, User, UserGroup}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class GroupService {

  import group.feed.service.db.DatabaseConfig._
  import group.feed.service.db.DatabaseConfig.jdbcProfile.api._

  val groupTable = tables.groups
  val userGroupsTable = tables.userGroups

  def getGroups()(implicit user: User): Future[Seq[Group]] = {
    val groupsUserBelongsTo = for {
      (g, _) <- groupTable join userGroupsTable.filter(_.userId === user.id) on (_.id === _.groupId)
    } yield (g)
    db.run(groupsUserBelongsTo.result)
  }

  def addUserToGroup(groupId: Long)(implicit user: User): Future[Group] = {
    val insertGroupAndUserGroup = (for {
      g <- groupTable returning groupTable.map(_.id) into ((group, id) => group.copy(id = id)) += Group(groupId, s"group$groupId")
      _ <- userGroupsTable.insertOrUpdate(UserGroup(None, user.id, g.id))
    } yield (g)).transactionally
    db.run(insertGroupAndUserGroup)
  }
}

object GroupService {
  def apply() = new GroupService()
}
