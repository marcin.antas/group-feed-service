package group.feed.service.service

import group.feed.service.model.{Post, User}
import org.joda.time.DateTime

import scala.concurrent.Future

class PostService {

  import group.feed.service.db.DatabaseConfig._
  import group.feed.service.db.DatabaseConfig.jdbcProfile.api._

  val postsTable = tables.posts
  val userGroupsTable = tables.userGroups

  def getAllGroupsPosts()(implicit user: User): Future[Seq[Post]] = {
    db.run(postsTable
      .filter(_.groupId in userGroupsTable.filter(_.userId === user.id).map(_.groupId))
      .result)
  }

  def getGroupPosts(groupId: Long)(implicit user: User): Future[Seq[Post]] = {
    db.run(postsTable
      .filter(_.groupId in userGroupsTable.filter(ug => ug.userId === user.id && ug.groupId === groupId).map(_.groupId))
      .sortBy(_.id.desc)
      .result)
  }

  def createPostInGroup(post: Post, groupId: Long)(implicit user: User): Future[Post] = {
    db.run(postsTable returning postsTable.map(_.id) into ((post, id) => post.copy(id = Some(id))) +=
        Post(None, post.content, Option(DateTime.now()), Option(user.id), Option(user.name), Option(groupId)))
  }
}

object PostService {
  def apply() = new PostService()
}