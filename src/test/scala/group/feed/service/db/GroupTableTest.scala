package group.feed.service.db

import com.typesafe.config.ConfigFactory
import com.typesafe.sslconfig.util.ConfigLoader
import group.feed.service.model.Group
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
import slick.jdbc.meta._

import scala.concurrent.duration._
import scala.concurrent.Await

class GroupTableTest extends FlatSpec with BeforeAndAfter {

  var db: Database = _
  implicit var session: Session = _
  object tables extends Tables(H2Profile)
  val groups = TableQuery[tables.Groups]

  before {
    db = Database.forConfig("testdb", config = ConfigFactory.load("db-application"))
    session = db.createSession()
  }

  "Operations using Group entity" should "Work" in {
    createSchema()
    val tables = Await.result(db.run(MTable.getTables), 2 seconds)
    assert(tables.size == 1)
    Await.result(db.run(groups += Group(0, name = "Group1")), 2 seconds)
    Await.result(db.run(groups += Group(0, name = "Group2")), 2 seconds)

    val q = for {
      h <- groups
    } yield(h)

    val groupsResult = Await.result(db.run(groups.result), 2 seconds)
    println(groupsResult)
  }

  def createSchema() =
    Await.result(db.run((groups.schema).create), 2 seconds)

  after { db.close }

}
